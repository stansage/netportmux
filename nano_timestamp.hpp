#pragma once

#include <cstdint>

class NanoTimestamp
{
public:
    static std::uint64_t now();

};
