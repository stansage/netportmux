#pragma once

#include "grabber.hpp"

class NixGrabber : public Grabber
{
public:
    NixGrabber( Forwarder & forwarder );

private:
    void startGrab( const char * device,
                    const char * filter,
                    const bool promiscuous ) override;
    void stopGrab() override;
};
