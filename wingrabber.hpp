#pragma once

#include "grabber.hpp"

class WinGrabber : public Grabber
{
public:
    WinGrabber( Forwarder & forwarder );

private:
    void startGrab( const char * device,
                    const char * filter,
                    const bool promiscuous ) override;
    void stopGrab() override;

private:
    bool m_exit;
    void * m_quit;
};
