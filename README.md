# NetPortMux

Receive network packets from port and forward to others.

Install dependencies as **Boost v1.58** and **WinPcap** on Windows or **libpcap** on Linux
Use **CMake 3.0** to build project.

By default the configuration file name is **netportmux.conf** and it must be placed to program working directory.
And you can set the file path via the program enviroment variable **NET_PORT_MUX_CONFIG**. File Format is INI.
Available options are:

* device - network device name to grab packets from
* in_port - TCP or UDP port to filter incoming packets
* outputs - Outgoing endpoints in format **host:port** delimited by commas

Please note, that output ports is UDP only.

If config file is empty or not found then default values given:
device = any
in_port = 31415
outputs = 127.0.0.1:12345,127.0.0.1:54321
