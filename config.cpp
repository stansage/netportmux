#include "config.hpp"
#include <boost/algorithm/string.hpp>
#include <fstream>
#include <iostream>

Config loadConfig()
{
    std::fstream fconf;
    const auto configName = "netportmux.conf";
    auto conf = static_cast<const char *>( std::getenv( "NET_PORT_MUX_CONFIG" ) );
    Config  result {
        { "device", "" },
        { "in_port", "31415" },
        { "outputs", "127.0.0.1:12345,127.0.0.1:54321" }
    };

    if ( conf == nullptr ) {
        conf = configName;
    }

    fconf.open( conf, std::ios::in );

    if ( fconf.is_open() ) {
        std::string line;
        while ( std::getline( fconf, line ).good() ) {
            if ( ! line.empty() ) {
                const auto idx = line.find( '=' );
                if ( idx != std::string::npos ) {
                    const auto key = boost::trim_copy( line.substr( 0, idx ) );
                    const auto it = result.find( key );
                    if ( it != result.end() ) {
                        it->second.assign( boost::trim_copy( line.substr( idx + 1 ) ) );
                    }
                }
            }
        }
    }

    return result;
}
