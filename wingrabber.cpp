#include "wingrabber.hpp"
#include "nano_timestamp.hpp"
//#define WIN32
#define WPCAP
#include <pcap/pcap.h>
#include <array>
#include <iostream>

WinGrabber::WinGrabber( Forwarder & forwarder ) :
    Grabber{ forwarder },
    m_exit{ false }
{

}


void WinGrabber::startGrab( const char * device,
                            const char * filter,
                            const bool promiscuous )
{
    constexpr auto bufferSize = 65536;
    std::array<char, PCAP_ERRBUF_SIZE> errbuf;

    m_quit = CreateEvent( nullptr, FALSE, FALSE, nullptr );

    if ( m_quit == nullptr ) {
        throw std::runtime_error( "couldn't create event" );
    }

    if ( pcap_setnonblock( handle(), 1, errbuf.data() ) != 0 ) {
        std::cerr << "warning: can't set non blcoking mode for device " << device << " - " << errbuf.data() << std::endl;
    }
    if ( pcap_set_snaplen( handle(), bufferSize ) != 0 ) {
        std::cerr << "warning: can't set snap size for device " << device << " - " << lastError() << std::endl;
    }
    if ( pcap_setbuff( handle(), bufferSize ) != 0 ) {
        std::cerr << "warning: can't set buffer for device " << device << " - " << lastError() << std::endl;
    }
    if ( pcap_set_buffer_size( handle(), bufferSize ) != 0 ) {
        std::cerr << "warning: can't set buffer size for device " << device << " - " << lastError() << std::endl;
    }
    if ( pcap_setmintocopy( handle(), 0 ) != 0 ) {
        std::cerr << "warning: can't set minimum size for device " << device << " - " << lastError() << std::endl;
    }
    if ( pcap_set_promisc( handle(), promiscuous ? 1 : 0 ) != 0 ) {
        std::cerr << "warning: can't promiscuous mode for device " << device << " - " << lastError() << std::endl;
    }
    if ( pcap_set_timeout( handle(), 1 ) != 0 ) {
        std::cerr << "warning: can't set timeout for device " << device << " - " << lastError() << std::endl;
    }
    if ( pcap_activate( handle() ) != 0 ) {
        throw std::runtime_error( "couldn't activate device capturing " + std::string( pcap_geterr( handle() ) ) );
    }
    HANDLE events[]{ pcap_getevent( handle() ),
                     m_quit };

    if ( events[ 0 ] == nullptr ) {
        throw std::runtime_error( "couldn't receive events from device " + std::string( device ) );
    }

    setFilter( device, filter );


    while ( ! m_exit && pcap_loop( handle(), 1, gotPacket, reinterpret_cast<u_char*>( this ) ) == 0 ) {
        std::cout << "got packet " << lastError() << std::endl;
    }
    std::cout << " exit loop " << lastError() << std::endl;

//    while ( ! m_exit && WaitForMultipleObjects( 2, events, FALSE, -1 ) != WAIT_FAILED ) {
//        std::cout << "reading packet by " << NanoTimestamp::now() << std::endl;

//        if ( ! m_exit && pcap_dispatch( handle(), 1, gotPacket, reinterpret_cast<u_char*>( this ) ) != 0 ) {
//            std::cerr << "warning: can't get packet from device " << device << " - " << lastError() << std::endl;
//        }
//    }

    CloseHandle( m_quit );
    m_quit = nullptr;
}

void WinGrabber::stopGrab()
{
    m_exit = true;
    SetEvent( m_quit );
    pcap_breakloop( handle() );
}
