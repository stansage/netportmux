#pragma once

struct pcap;
struct pcap_pkthdr;

class Forwarder;

class Grabber
{
public:
    Grabber( Forwarder & forwarder );
    virtual ~Grabber();

    void start( const char * device,
                const char * filter );
    void stop();

protected:
    pcap * handle() const;
    void setFilter( const char * device,
                    const char * filter );
    const char * lastError() const;
    static void gotPacket( unsigned char * user,
                           const pcap_pkthdr * header,
                           const unsigned char * packet );

private:
    virtual void startGrab( const char * device,
                            const char * filter,
                            const bool promiscuous ) = 0;
    virtual void stopGrab() = 0;

private:
    pcap * m_handle;
    Forwarder & m_forwarder;
};
