#pragma once

#include <unordered_map>

using Config = std::unordered_map<std::string, std::string>;

Config loadConfig();
