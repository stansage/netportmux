#include "nano_timestamp.hpp"

#ifdef _WIN32_WINNT
#include "Windows.h"
#else // ! _WIN32_WINN
#include <time.h>
#endif // _WIN32_WINNT

uint64_t NanoTimestamp::now()
{
        auto result = 0ull;

#ifdef _WIN32_WINNT
        FILETIME ft = {};
        ULARGE_INTEGER ts = {};

        GetSystemTimePreciseAsFileTime( & ft );
        ts.LowPart = ft.dwLowDateTime;
        ts.HighPart = ft.dwHighDateTime;

        result = ts.QuadPart;
        result -= 116444736000000000ull;
        result *= 100;

//        LARGE_INTEGER currentTime;
//        LARGE_INTEGER frequency{};

//        QueryPerformanceFrequency( & frequency );
//        QueryPerformanceCounter( & currentTime );

//        result = currentTime.QuadPart;
//        result *= 1000000000;
//        result /= Frequency.QuadPart;
#else // ! _WIN32_WINNT
        timespec ts = {};

        if ( clock_gettime( CLOCK_REALTIME, & ts ) == 0 ) {
            result = ts.tv_sec;
            result *= 1000000000;
            result += ts.tv_nsec;
        }
#endif // _WIN32_WINNT

        return result;
    }
