#include "grabber.hpp"
#include "forwarder.hpp"
#include "nano_timestamp.hpp"
#define WPCAP
#define HAVE_REMOTE
#include <pcap/pcap.h>
#include <memory>
#include <iostream>

Grabber::Grabber( Forwarder & forwarder ) :
    m_handle( nullptr ),
    m_forwarder( forwarder )
{
}

Grabber::~Grabber()
{
    stop();
}

void Grabber::start( const char * device,
                     const char * filter )
{
//    pcap_if_t *alldevs;
//    pcap_if_t *d;
//    int inum;
//    int i=0;
//    pcap_t *adhandle;
//    int res;
//    char errbuf[PCAP_ERRBUF_SIZE];
//    struct tm *ltime;
//    char timestr[16];
//    struct pcap_pkthdr *header;
//    const u_char *pkt_data;
//    time_t local_tv_sec;


//        /* Retrieve the device list on the local machine */
//        if (pcap_findalldevs_ex(PCAP_SRC_IF_STRING, NULL, &alldevs, errbuf) == -1)
//        {
//            fprintf(stderr,"Error in pcap_findalldevs: %s\n", errbuf);
//            exit(1);
//        }

//        /* Print the list */
//        for(d=alldevs; d; d=d->next)
//        {
//            printf("%d. %s", ++i, d->name);
//            if (d->description)
//                printf(" (%s)\n", d->description);
//            else
//                printf(" (No description available)\n");
//        }

//        if(i==0)
//        {
//            printf("\nNo interfaces found! Make sure WinPcap is installed.\n");
//            return ;
//        }

//        inum = 1;
////        printf("Enter the interface number (1-%d):",i);
////        scanf("%d", &inum);

////        if(inum < 1 || inum > i)
////        {
////            printf("\nInterface number out of range.\n");
////            /* Free the device list */
////            pcap_freealldevs(alldevs);
////            return ;
////        }

//        /* Jump to the selected adapter */
//        for(d=alldevs, i=0; i< inum-1 ;d=d->next, i++);

//        /* Open the device */
//        if ( (adhandle= pcap_open(d->name,          // name of the device
//                                  65536,            // portion of the packet to capture.
//                                                    // 65536 guarantees that the whole packet will be captured on all the link layers
//                                                    //
//                                  PCAP_OPENFLAG_MAX_RESPONSIVENESS | PCAP_OPENFLAG_PROMISCUOUS,    // promiscuous mode
//                                  5000,                // read timeout
//                                  NULL,             // authentication on the remote machine
//                                  errbuf            // error buffer
//                                  ) ) == NULL)
//        {
//            fprintf(stderr,"\nUnable to open the adapter. %s is not supported by WinPcap\n", d->name);
//            /* Free the device list */
//            pcap_freealldevs(alldevs);
//            return ;
//        }

//        printf("\nlistening on %s...\n", d->description);


//        /* At this point, we don't need any more the device list. Free it */
//        pcap_freealldevs(alldevs);

//        m_handle = adhandle;
//        setFilter( device, filter );


//        /* Retrieve the packets */
//        while((res = pcap_next_ex( adhandle, &header, &pkt_data)) >= 0){

//            if(res == 0) {
//                printf("read timeout\n");
//                /* Timeout elapsed */
//                continue;
//            }

//            /* convert the timestamp to readable format */
////            local_tv_sec = header->ts.tv_sec;
////            ltime=localtime(&local_tv_sec);
////            strftime( timestr, sizeof timestr, "%H:%M:%S", ltime);

//            printf("%.6ld.%.6ld len:%u %lu\"", header->ts.tv_sec, header->ts.tv_usec, header->len, NanoTimestamp::now());
//            for(int i=0; i<header->len; ++i) {
//                printf("%c", pkt_data[i]);
//            }
//            printf("\"\n");
//        }

//        if(res == -1){
//            printf("Error reading the packets: %s\n", pcap_geterr(adhandle));
//            return;
//        }

//    return;

    BOOST_ASSERT( m_handle == nullptr );

    std::cout << "grabbing device " << device << std::endl;

    std::array<char, PCAP_ERRBUF_SIZE> errbuf;
    const std::string any = "any";
    const auto nodev = device == nullptr || * device == '\0';
    const auto anydev = ! nodev && any.compare( device ) == 0;

    if ( ! nodev && ! anydev ) {
        pcap_if_t * dev = nullptr;
        pcap_if_t * devs = nullptr;
        std::string devlist;

        if ( pcap_findalldevs( & devs, errbuf.data() ) != 0 ) {
            throw std::runtime_error( "couldn't find any network device " +
                                      std::string( errbuf.data() ) );
        }

        for ( dev = devs; dev != nullptr; dev = dev->next ) {
            if ( nodev ) {
                devlist += dev->name;
                if ( dev->next != nullptr ) {
                    devlist += '\n';
                }
            } else if ( std::strcmp( dev->name, device ) == 0 ) {
                break;
            }
        }
        pcap_freealldevs( devs );

        if ( nodev ) {
            throw std::runtime_error( "you must select network device from list:\n" + devlist );
        }
        if ( dev == nullptr ) {
            throw std::runtime_error( "couldn't find selected device " );
        }
    }

    m_handle = pcap_create( device, errbuf.data() );
//    handle() = pcap_open_live( devname, 8096, promiscuous, 1000, errbuf.data() );

    if ( m_handle == nullptr ) {
        throw std::runtime_error( "couldn't open network device " + std::string( errbuf.data() ) );
    }

    startGrab( device, filter, ! nodev && ! anydev );
}

void Grabber::stop()
{
    if ( m_handle != nullptr ) {
        stopGrab();
        pcap_close( m_handle );
        m_handle = nullptr;
    }
}

pcap * Grabber::handle() const
{
    return m_handle;
}

void Grabber::setFilter( const char * device,
                         const char * filter )
{
    std::array<char, PCAP_ERRBUF_SIZE> errbuf;
    bpf_u_int32 mask = 0; /// The netmask of our sniffing device
    bpf_u_int32 net = 0; /// The IP of our sniffing device
    bpf_program program = {};

    if ( pcap_lookupnet( device, & net, & mask, errbuf.data() ) != 0 ) {
        std::cerr << "warning: can't get netmask for device " << device << ": " << errbuf.data() << std::endl;
    }

    std::cout << "installing filter " << filter << " for device " << device  << " on net " << net << " and mask " << mask << std::endl;

    if ( pcap_compile( m_handle, & program, filter, 1, mask ) != 0 ) {
        throw std::runtime_error( std::string( "couldn't parse filter " ) + filter + std::string( " - " ) + lastError() );
    }
    if ( pcap_setfilter( m_handle, & program ) != 0 ) {
        throw std::runtime_error( std::string( "couldn't install filter " ) + filter + std::string( " - " ) + pcap_geterr( m_handle ) );
    }
}

const char * Grabber::lastError() const
{
    return pcap_geterr( m_handle );
}

void Grabber::gotPacket( unsigned char * user,
                         const pcap_pkthdr * header,
                         const unsigned char * packet )
{
    const auto self = reinterpret_cast<Grabber*>( user );
    BOOST_ASSERT( self != nullptr && self->m_handle != nullptr );

    if ( header->len > 0 ) {
        std::cout << NanoTimestamp::now() <<  ": \"";
        for ( auto i = 0; i < header->len; ++i ) {
            std::cout << static_cast<char>( packet[ i ] );
        }
        std::cout << "\" jacked a packet with length of " << header->caplen << '(' << header->len << ')' << std::endl;
        self->m_forwarder.send( packet, header->len );
    }
}
