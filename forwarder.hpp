#pragma once

#include <boost/asio/ip/udp.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/lockfree/queue.hpp>
#include <queue>
#include <map>

class Forwarder
{
    using Udp = boost::asio::ip::udp;
    using Packet = std::pair<std::vector<u_char>, std::vector<std::size_t>>;

public:
    explicit Forwarder( boost::asio::io_service & ios,
                        const std::string & outputs );

    void send( const u_char * data,
               const std::size_t size );

private:
    void writeHandler( Packet * packet,
                       const int index,
                       const boost::system::error_code error,
                       const std::size_t bytesWritten );

private:
    Udp::socket m_socket;
    boost::lockfree::queue<Packet*> m_queue;
    std::vector<Udp::endpoint> m_endpoints;
};
