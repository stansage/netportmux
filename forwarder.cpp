#include "forwarder.hpp"
#include <boost/bind/bind.hpp>
#include <boost/asio/placeholders.hpp>
#include <iostream>
#include <iomanip>

Forwarder::Forwarder( boost::asio::io_service & ios,
                      const std::string & outputs ) :
    m_socket( ios ),
    m_queue{ 512 }
{
    std::string output;
    std::stringstream stream( outputs );

    while ( std::getline( stream, output, ',' ) ) {
        const auto idx = output.find( ':' );
        if ( idx != std::string::npos ) {
            const auto address = boost::asio::ip::address::from_string( output.substr( 0, idx ) );
            const auto port = std::stoi( output.substr( idx + 1 ) );
            m_endpoints.emplace_back( address, port );
        }
    }

    m_socket.open( Udp::v4() );
}

void Forwarder::send( const u_char * data,
                      const std::size_t size )
{
    using namespace boost::asio::placeholders;

    auto packetPtr = std::make_unique<Packet>();
    packetPtr->first.assign( data, data + size );
    packetPtr->second.resize( m_endpoints.size() );

    if ( ! m_queue.push( packetPtr.get() ) ) {
        std::cerr << "warning: skipping packet due the queue size limit exceeded" << std::endl;
    } else {
        const auto packet = packetPtr.release();

        for ( auto index = 0; index < m_endpoints.size(); ++index ) {
            std::cout << "sending " << size << " bytes to " << m_endpoints[ index ] << std::endl;
//            std::cout << "packet: {";
//            for ( const auto it : { data, data + size } ) {
//                std::cout << std::setw( 2 ) << std::setfill( '0' ) << std::hex << static_cast<int>( * it );
//            }
//            std::cout << '}' << std::endl;

            m_socket.async_send_to( boost::asio::buffer( packet->first ), m_endpoints[ index ],
                                    boost::bind( & Forwarder::writeHandler, this, packet, index, error, bytes_transferred ) );
        }
    }
}

void Forwarder::writeHandler( Packet * packet,
                              const int index,
                              const boost::system::error_code error,
                              const std::size_t bytesWritten )
{
    if ( error ) {
        packet->second.at( index ) = packet->first.size();
        std::cerr << "failed to send packet to " << m_endpoints[ index ] << std::endl;
    } else {
        packet->second.at( index ) += bytesWritten;
        std::cout << "successfully transferred " << bytesWritten << " bytes to " << m_endpoints[ index ] << std::endl;
    }

    if ( m_endpoints.size() == std::count( packet->second.begin(), packet->second.end(), packet->first.size() ) ) {
        Packet * packet = nullptr;
        if ( m_queue.pop( packet  ) ) {
            delete packet;
        }
    }
}


