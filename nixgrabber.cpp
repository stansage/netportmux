#include "nixgrabber.hpp"
#include <pcap/pcap.h>
#include <array>
#include <iostream>

NixGrabber::NixGrabber( Forwarder & forwarder ) :
    Grabber{ forwarder }
{

}

void NixGrabber::startGrab( const char * device,
                            const char * filter,
                            const bool promiscuous )
{
    if ( pcap_set_immediate_mode( handle(), 4096 ) != 0 ) {
        std::cerr << "warning: can't set immediate mode for device " << device << " - " << pcap_geterr( handle() ) << std::endl;
    }

    if ( pcap_set_promisc( handle(), promiscuous ? 1 : 0 ) != 0 ) {
        std::cerr << "warning: can't set promiscuous mode for device " << device << " - " << pcap_geterr( handle() ) << std::endl;
    }
    if ( pcap_set_snaplen( handle(), 65536 ) != 0 ) {
        std::cerr << "warning: can't set snaplen for device " << device << " - " << pcap_geterr( handle() ) << std::endl;
    }
    if ( pcap_activate( handle() ) != 0 ) {
        throw std::runtime_error( "couldn't activate device capturing " + std::string( pcap_geterr( handle() ) ) );
    }
    if ( pcap_setdirection( handle(), PCAP_D_IN ) != 0 ) {
        std::cerr << "warning: can't set incoming direction for device " << device << " - " << pcap_geterr( handle() ) << std::endl;
    }
    setFilter( device, filter );


    while ( pcap_loop( handle(), 1000, gotPacket, reinterpret_cast<u_char*>( this ) ) != -2 ) {
        std::cerr << "warning: capture packet  failed with error " << pcap_geterr( handle() ) << std::endl;
    }
}

void NixGrabber::stopGrab()
{
    pcap_breakloop( handle() );
}
