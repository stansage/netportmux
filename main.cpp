#include "config.hpp"
#include "forwarder.hpp"
#include "grabber.hpp"
#include <boost/atomic/atomic.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/signal_set.hpp>
#include <boost/thread/thread.hpp>
#include <iostream>

#ifdef _WIN32_WINNT
#include "wingrabber.hpp"
using GrabberImpl = WinGrabber;
#else
#include "nixgrabber.hpp"
using GrabberImpl = NixGrabber;
#endif // _WIN32_WINNT

int main()
{
    /// Setup the initial state
    boost::thread_group     tpool;
    boost::asio::io_service ios;
    boost::asio::signal_set sigs( ios, SIGINT, SIGTERM );
    boost::atomic_bool      exit( false );
    auto work               = std::make_unique<boost::asio::io_service::work>( ios );
    auto config             = loadConfig();
    auto result             = 0;

    /// Create the working threads to be used for socket communications
    exit = false;
    for( int x = 0; x < 2; ++x ) {
        tpool.create_thread( [ & ios, & exit, & result ]() {
            while ( ! exit ) {
                try {
                    ios.run();
                } catch ( const std::exception & exc ) {
                    std::cerr << "asio exception thrown " << exc.what() << std::endl;
                    result = 1;
                } catch ( ... ) {
                    std::cerr << "unhadled exception thrown" << std::endl;
                    result = 2;
                }
            }
        } );
    }

    /// Grab and fowrward packets to specified ports
    const auto stop = [ & ios, & exit ]() {
        while ( ! exit  ) {
            exit = true;
        }
        while ( ! ios.stopped() ) {
            ios.stop();
        }
    };
    try {
        Forwarder forwarder( ios, config[ "outputs" ] );
        GrabberImpl grabber( forwarder );

        /// Start an asynchronous wait for one of the signals to occur.
        sigs.async_wait( [ & stop, & grabber ]( const boost::system::error_code error,
                                const int signal ) {
            if ( error ) {
                std::cerr << "received error on signal " << signal << ": " << error;
            } else {
                std::cout << "shutting down by signal " << signal << std::endl;
            }
            stop();
            grabber.stop();
        } );

//        forwarder.send( reinterpret_cast<const u_char*>( "0123456789" ), 10 );

        grabber.start( config[ "device" ].c_str(), ( "port " + config[ "in_port" ] ).c_str() );
//        grabber.start( config[ "device" ].c_str(), ( "tcp or udp and dst port " + config[ "in_port" ] ).c_str() );
//        grabber.start( config[ "device" ].c_str(), "tcp or udp" );
        std::cout << "exiting" << std::endl;
    } catch ( const std::exception & exc ) {
        std::cerr << "aborting due error: " << exc.what() << std::endl;
        result = 3;
        stop();
    }

    /// Final cleanup
    std::cout << "reseting" << std::endl;
    work.reset();
    std::cout << "joining" << std::endl;
    tpool.join_all();

    return result;
}
